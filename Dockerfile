FROM python:3.9

WORKDIR /code

COPY ./requirements.txt /code/requirements.txt

RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt

COPY ./telecommown /code/telecommown
COPY ./bin /code/bin

CMD ["uvicorn", "telecommown.main:app", "--proxy-headers", "--host", "0.0.0.0", "--port", "80"]
