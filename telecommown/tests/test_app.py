from contextlib import contextmanager
import pprint

import pytest

from sqlalchemy_utils import database_exists
from fastapi import Request
from fastapi.testclient import TestClient

from ..database import engine, SessionLocal
from ..init import init_db
from ..main import app, get_db
from ..auth import get_username


client = TestClient(app)

FAKE_ID = object()


@pytest.fixture(scope="session", autouse=True)
def main_db_session():

    if not database_exists(engine.url):
        init_db()

    with SessionLocal.begin() as main_session:
        yield main_session


@pytest.fixture(scope="function", autouse=True)
def savepoint_test(main_db_session):

    with main_db_session.begin_nested() as test_nested:

        def override_get_subtransaction():
            api_nested = main_db_session.begin_nested()
            try:
                yield main_db_session
            finally:
                if api_nested.is_active:
                    main_db_session.commit()  # release the savepoint

        app.dependency_overrides[get_db] = override_get_subtransaction

        try:
            yield
        finally:
            test_nested.rollback()


@pytest.fixture
def members():
    with dep_overrides(get_username, AsUser("admin")):
        user_ids = create_users("Telecoop", "Commown")
    return user_ids


@pytest.fixture
def campaign(members):
    with dep_overrides(get_username, AsUser("admin")):
        campaign = check_resp(
            client.post("/campaigns/", json={
                "ref": "test-campaign",
                "member_ids": list(members.values()),
                "start_ts": "2022-01-01T00:00:00+00:00",
                "end_ts": "2023-01-01T00:00:00+00:00"})
        ).json()
    return campaign


@pytest.fixture
def subscriptions(campaign):
    t1, t2 = "T10:00:00+01:00", "T11:00:00+02:00"
    subscriptions = {
        "customer-1": {
            "Telecoop": ("2022-02-01" + t1, None),
            "Commown":  ("2022-04-15" + t2, "2022-09-01" + t2),
        },
        "customer-2": {
            "Telecoop": ("2022-07-01" + t2, "2022-11-01" + t1),
            "Commown":  ("2022-02-01" + t1, "2022-09-01" + t2),
        },
        "customer-3": {
            "Telecoop": ("2022-04-01" + t2, "2022-06-01" + t2),
            "Commown":  ("2022-07-01" + t2, "2022-09-01" + t2),
        },
        "customer-4": {
            "Telecoop": ("2022-02-01" + t1, "2022-06-01" + t2),
        },
        "customer-5": {
            "Telecoop": ("2022-02-01" + t1, None),
            "Commown":  ("2022-03-01" + t1, None),
        },
    }

    for key, partners in subscriptions.items():
        for partner, (optin, optout) in partners.items():
            with dep_overrides(get_username, AsUser(partner)):
                if optin is not None:
                    check_resp(client.post(
                        "/campaigns/%s/opt-in" % campaign["ref"],
                        json={"customer_key": key, "optin_ts": optin})
                    )
                if optout is not None:
                    check_resp(client.post(
                        "/campaigns/%s/opt-out" % campaign["ref"],
                        json={"customer_key": key, "optout_ts": optout})
                    )
    return subscriptions


def replace_key(json_obj, key="id", value=FAKE_ID):
    '''Replace all keys named `key` in the dicts found in `json_obj` python obj
    by given `value`.

    This object must be simple enough for this function to work. Typical
    HTTP API responses should work.
    '''
    if isinstance(json_obj, dict):
        if key in json_obj:
            json_obj[key] = value
        for new_json_obj in json_obj.values():
            replace_key(new_json_obj, key=key, value=value)
    elif isinstance(json_obj, list):
        for new_json_obj in json_obj:
            replace_key(new_json_obj, key=key, value=value)
    return json_obj


def check_resp(resp, expect_json=None, expect_status=200,
               post_processors=(replace_key,)):
    assert resp.status_code == expect_status, "\n".join(
        [resp.reason, pprint.pformat(resp.json())])
    if expect_json is not None:
        json = resp.json()
        for processor in post_processors:
            json = processor(json)
        assert json == expect_json
    return resp


def create_users(*logins):
    user_ids = {}
    for login in logins:
        user = check_resp(client.post("/users/", json={"login": login})).json()
        user_ids[user["login"]] = user["id"]
    return user_ids


@contextmanager
def dep_overrides(initial, substitute):
    old_value = app.dependency_overrides.pop(initial, None)
    app.dependency_overrides[initial] = substitute
    try:
        yield
    finally:
        if old_value is None:
            del app.dependency_overrides[initial]
        else:
            app.dependency_overrides[initial] = old_value


class AsUser():

    def __init__(self, login):
        self.login = login

    def __call__(self, request: Request):
        request.state.user = self.login
        return self.login


def test_no_auth():
    check_resp(client.get("/users/"), expect_status=401)


def test_users():

    with dep_overrides(get_username, AsUser("toto")):
        check_resp(client.get("/users/"), [])

        with dep_overrides(get_username, AsUser("admin")):
            create_users("Telecoop", "Commown")

        resp = client.get("/users/")
        check_resp(resp, [{"id": FAKE_ID, "login": "Telecoop"},
                          {"id": FAKE_ID, "login": "Commown"}])

        check_resp(client.get("/users/%s" % resp.json()[1]["id"]),
                   {"id": FAKE_ID, "login": "Commown"})

        with dep_overrides(get_username, AsUser("admin")):
            check_resp(client.post("/users/", json={"login": "Commown"}),
                       {"detail": "User already registered"},
                       expect_status=400)

        check_resp(client.get("/users/"),
                   [{"id": FAKE_ID, "login": "Telecoop", },
                    {"id": FAKE_ID, "login": "Commown"}])


def test_campaigns(members):
    with dep_overrides(get_username, AsUser("toto")):
        check_resp(client.get("/campaigns/"), [])

    with dep_overrides(get_username, AsUser("Telecoop")):
        check_resp(
            client.post("/campaigns/", json={
                "ref": "test-campaign-1",
                "display_name": "Licoornes Campaign",
                "member_ids": list(members.values()),
                "start_ts": "2021-09-01T09:00:00",
                "end_ts": "2021-12-31T23:59:59"})
        )

        member_ids_but_telecoop = [
            v for k, v in members.items() if k != "Telecoop"
        ]
        check_resp(
            client.post("/campaigns/", json={
                "ref": "test-campaign-2",
                "display_name": "TC Campaign",
                "member_ids": member_ids_but_telecoop,
                "start_ts": "2021-09-01T09:00:00",
                "end_ts": "2021-12-31T23:59:59"}),
            expect_status=401,
        )


def test_opt_in_and_out(campaign):

    with dep_overrides(get_username, AsUser("Telecoop")):
        # Successful opt-in
        check_resp(
            client.post("/campaigns/test-campaign/opt-in", json={
                "customer_key": "customer-1",
                "optin_ts": "2022-06-01T11:00:00+02:00",
            }),
            {
                "id": FAKE_ID,
                "customer_key": "customer-1",
                "optin_ts": "2022-06-01T11:00:00+02:00",
                "optout_ts": None,
                "campaign": replace_key(campaign),
                "member": {"login": "Telecoop", "id": FAKE_ID},
            },
        )

        # Must be possible to register subscription before the campaign start:
        check_resp(
            client.post("/campaigns/test-campaign/opt-in", json={
                "customer_key": "customer-2",
                # campaign not started:
                "optin_ts": "2020-01-01T09:00:00+00:00",
            }),
        )

        # Opt-in: already registered
        check_resp(
            client.post("/campaigns/test-campaign/opt-in", json={
                "customer_key": "customer-1",
                "optin_ts": "2021-12-01T10:00:00+01:00",
            }),
            expect_status=422,
            expect_json={'detail': 'Already opt-in'},
        )

        # Opt-in: invalid date errors
        check_resp(
            client.post("/campaigns/test-campaign/opt-in", json={
                "customer_key": "customer-error",
                # campaign ended:
                "optin_ts": "2023-01-01T09:00:00+00:00",
            }),
            expect_status=422,
        )

        # Opt-out: invalid date errors
        check_resp(
            client.post("/campaigns/test-campaign/opt-out", json={
                "customer_key": "customer-1",
                # campaign ended:
                "optout_ts": "2023-01-01T09:00:00+00:00",
            }),
            expect_status=422,
        )

        check_resp(
            client.post("/campaigns/test-campaign/opt-out", json={
                "customer_key": "customer-1",
                # before opt-in:
                "optout_ts": "2022-05-31T09:00:00+00:00",
            }),
            expect_status=422,
        )

        # Without error
        check_resp(
            client.post("/campaigns/test-campaign/opt-out", json={
                "customer_key": "customer-1",
                "optout_ts": "2022-08-25T09:00:00+00:00",
            }),
        )

        # Already opt-out
        check_resp(
            client.post("/campaigns/test-campaign/opt-out", json={
                "customer_key": "customer-1",
                "optout_ts": "2022-12-01T09:00:00+00:00",
            }),
            expect_status=422,
        )


def important_events(since, until, check_detail_nb=None, remove_details=True,
                     hour="T09:00:00+00:00", customer_key=None):
    params = {"since": since+hour, "until": until+hour}
    if customer_key is not None:
        params["customer_key"] = customer_key
    json = check_resp(
        client.get(
            "/campaigns/test-campaign/subscriptions/important-events",
            params=params,
        )
    ).json()
    if check_detail_nb is not None:
        for item in json:
            assert len(item["details"]) == check_detail_nb
    if remove_details:
        for item in json:
            del item["details"]
    return json


def test_subscription_important_events(subscriptions):

    with dep_overrides(get_username, AsUser("Commown")):
        assert (
            important_events("2021-12-31", "2023-01-02", 2)
            == [{'customer_key': 'customer-1',
                 'events': [
                     {'ts': '2022-04-15T11:00:00+02:00', 'type': 'optin'},
                     {'ts': '2022-09-01T11:00:00+02:00', 'type': 'optout'}
                 ]},
                {'customer_key': 'customer-2',
                 'events': [
                     {'ts': '2022-07-01T11:00:00+02:00', 'type': 'optin'},
                     {'ts': '2022-09-01T11:00:00+02:00', 'type': 'optout'}
                 ]},
                {'customer_key': 'customer-3',
                 'events': [
                     {'ts': '2022-07-01T11:00:00+02:00', 'type': 'optin'},
                     {'ts': '2022-06-01T11:00:00+02:00', 'type': 'optout'}
                 ]},
                {'customer_key': 'customer-5',
                 'events': [
                     {'ts': '2022-03-01T10:00:00+01:00', 'type': 'optin'},
                     {'ts': '2023-01-01T01:00:00+01:00', 'type': 'optout'}
                 ]}]
        )

        assert (
            important_events("2021-12-31", "2023-01-02", 2,
                             customer_key="customer-1")
            == [{'customer_key': 'customer-1',
                 'events': [
                     {'ts': '2022-04-15T11:00:00+02:00', 'type': 'optin'},
                     {'ts': '2022-09-01T11:00:00+02:00', 'type': 'optout'}
                 ]}]
        )

        assert important_events("2022-01-31", "2022-02-07", 2) == []

        assert (
            important_events("2022-06-27", "2022-07-03", 2)
            == [
                {'customer_key': 'customer-2',
                 'events': [
                     {'ts': '2022-07-01T11:00:00+02:00',
                      'type': 'optin'}
                 ]},
                {'customer_key': 'customer-3',
                 'events': [
                     {'ts': '2022-07-01T11:00:00+02:00',
                      'type': 'optin'}
                 ]}]
        )

        assert (
            important_events("2022-05-30", "2022-06-01", 2)
            == [
                {'customer_key': 'customer-3',
                 'events': [
                     {'ts': '2022-06-01T11:00:00+02:00',
                      'type': 'optout'}
                 ]}]
        )

        assert (
            important_events("2022-12-30", "2023-01-02", 2)
            == [
                {'customer_key': 'customer-5',
                 'events': [
                     {'ts': '2023-01-01T01:00:00+01:00',
                      'type': 'optout'}
                 ]}]
        )


def test_campaign_subscriptions(campaign, subscriptions):
    fake_campaign = replace_key(campaign)
    expected_json = [
        {
            "id": FAKE_ID,
            "campaign": fake_campaign,
            "customer_key": customer_key,
            "member": {"id": FAKE_ID, "login": member},
            "optin_ts": optin_ts,
            "optout_ts": optout_ts,
        }
        for customer_key, members in subscriptions.items()
        for member, (optin_ts, optout_ts) in members.items()
    ]

    def sort_by_key(obj):
        if isinstance(obj, list):
            return sorted(obj, key=lambda d: d["customer_key"])
        else:
            return obj

    def check_subscriptions(expect_json=None, expect_status=200, **params):
        return check_resp(
            client.get("/campaign/%s/subscriptions" % campaign["ref"],
                       params=params),
            expect_json=expect_json,
            expect_status=expect_status,
            post_processors=(sort_by_key, replace_key),
        )

    with dep_overrides(get_username, AsUser("Commown")):
        # get all subscriptions
        check_subscriptions(expected_json)

        # error checks: since and until must be both absent or present
        check_subscriptions(None, 422, since="2022-06-15T00:00:00+00:00")

        # check date and customer key filters
        since, until = "2022-04-01T00:00:00+00:00", "2022-05-01T00:00:00+00:00"
        expected = [s for s in expected_json if since <= s["optin_ts"] < until]
        assert len(expected) == 2
        check_subscriptions(expected, since=since, until=until)

        key = "customer-1"
        expected = [
            s for s in expected_json
            if since <= s["optin_ts"] < until and s["customer_key"] == key
        ]
        assert len(expected) == 1
        check_subscriptions(expected, since=since, until=until,
                            customer_key=key)

        expected = [s for s in expected_json if s["customer_key"] == key]
        assert len(expected) == 2
        check_subscriptions(expected, customer_key=key)
