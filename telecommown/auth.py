from fastapi import Header, Request, HTTPException


def get_username(request: Request, x_forwarded_user: str = Header(None)):
    """ Set user login from HTTP X-Forwarded-User on request.state.user
    and return it, to make it usable as a simple dependency or a global
    dependency (in which case it is easier to use request).

    For some reason the value's encoding is wrong and could be fixed using::

      if x_forwarded_user is not None:
          x_forwarded_user = x_forwarded_user.encode("latin1").decode("utf-8")

    ... but not sure we should.
    """
    if x_forwarded_user is None:
        raise HTTPException(status_code=401, detail="Authentication required")
    request.state.user = x_forwarded_user
    return x_forwarded_user
