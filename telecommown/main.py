from datetime import datetime
from typing import List

from fastapi import Depends, FastAPI, HTTPException, Request
from sqlalchemy.orm import Session

from . import crud, schemas, models
from .database import SessionLocal, AlreadyExistsError
from .auth import get_username


# Secure all endpoints by default
app = FastAPI(dependencies=[Depends(get_username)])


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.post("/users/", response_model=schemas.User)
def create_user(user: schemas.UserCreate,
                request: Request,
                db: Session = Depends(get_db)):
    if request.state.user != "admin":
        raise HTTPException(status_code=401, detail="Must be admin to do this")
    db_user = crud.get_user_by_login(db, login=user.login)
    if db_user:
        raise HTTPException(status_code=400, detail="User already registered")
    return crud.create_user(db=db, user=user)


@app.get("/users/", response_model=List[schemas.User])
def read_users(db: Session = Depends(get_db)):
    users = crud.get_users(db)
    return users


@app.get("/users/{user_id}", response_model=schemas.User)
def read_user(user_id: int, db: Session = Depends(get_db)):
    db_user = crud.get_user(db, user_id=user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user


@app.get("/campaigns/", response_model=List[schemas.Campaign])
def read_campaigns(db: Session = Depends(get_db)):
    campaigns = crud.get_campaigns(db)
    return campaigns


@app.post("/campaigns/", response_model=schemas.Campaign)
def create_campaign(
        campaign: schemas.CampaignCreate,
        request: Request,
        db: Session = Depends(get_db)
):
    login = request.state.user
    if login != "admin" and crud.get_user_by_login(
            db, login).id not in campaign.member_ids:
        raise HTTPException(
            status_code=401,
            detail="Cannot create a campaign you're not a member of"
        )
    return crud.create_campaign(db=db, campaign=campaign)


def raise_if_not_member(
        login: str,
        campaign: models.Campaign,
        error_message: str,
        http_error_code: int = 401
) -> models.User:
    """ Raises an HTTP error if `login` is not a member of `campaign`
    or returns found campaign member.
    """
    for member in campaign.members:
        if login == member.login:
            break
    else:
        raise HTTPException(
            status_code=http_error_code,
            detail=error_message
        )
    return member


@app.post("/campaigns/{campaign_ref}/opt-in",
          response_model=schemas.Subscription)
def create_subscription(
        subscription: schemas.SubscriptionOptIn,
        campaign_ref: str,
        request: Request,
        db: Session = Depends(get_db)
):
    campaign = crud.get_campaign_by_ref(db=db, campaign_ref=campaign_ref)

    member = raise_if_not_member(
        request.state.user,
        campaign,
        "Cannot subscribe a customer to a campaign you're not a member of"
    )

    try:
        return crud.create_subscription(
            db=db,
            subscription=subscription,
            campaign=campaign,
            member=member,
        )
    except crud.InvalidDataError as exc:
        raise HTTPException(422, str(exc))
    except AlreadyExistsError as exc:
        raise HTTPException(422, "Already opt-in")


@app.post("/campaigns/{campaign_ref}/opt-out",
          response_model=schemas.Subscription)
def opt_out_subscription(
        subscription: schemas.SubscriptionOptOut,
        campaign_ref: str,
        request: Request,
        db: Session = Depends(get_db)
):
    campaign = crud.get_campaign_by_ref(db=db, campaign_ref=campaign_ref)

    member = raise_if_not_member(
        request.state.user,
        campaign,
        ("Cannot opt-out a customer subscription to a campaign"
         " you're not a member of")
    )

    try:
        return crud.opt_out_subscription(
            db=db,
            subscription=subscription,
            campaign=campaign,
            member=member,
        )
    except crud.InvalidDataError as exc:
        raise HTTPException(422, str(exc))


@app.get("/campaigns/{campaign_ref}/subscriptions/important-events",
         response_model=List[schemas.SubscriptionByCustomerKey])
def subscription_important_events(
        campaign_ref: str,
        request: Request,
        since: datetime = None,
        until: datetime = None,
        customer_key: str = None,
        db: Session = Depends(get_db)
):
    campaign = crud.get_campaign_by_ref(db=db, campaign_ref=campaign_ref)

    raise_if_not_member(
        request.state.user,
        campaign,
        ("Cannot opt-out a customer subscription to a campaign"
         " you're not a member of")
    )

    if bool(since) != bool(until):
        raise HTTPException(
            422,
            "since and until dates must be either both set or both unset",
        )

    if since is not None and until <= since:
        raise HTTPException(
            422,
            "since date must be strictly before until date",
        )

    return crud.subscription_important_events(
        db=db, campaign=campaign, since=since, until=until,
        customer_key=customer_key,
    )


@app.get("/campaign/{campaign_ref}/subscriptions",
         response_model=List[schemas.Subscription])
def campaign_subscriptions(
        campaign_ref: str,
        request: Request,
        since: datetime = None,
        until: datetime = None,
        customer_key: str = None,
        db: Session = Depends(get_db)
):
    campaign = crud.get_campaign_by_ref(db=db, campaign_ref=campaign_ref)

    raise_if_not_member(
        request.state.user,
        campaign,
        ("Cannot get subscriptions of a campaign you're not member of")
    )

    if bool(since) != bool(until):
        raise HTTPException(
            422,
            "since and until dates must be either both set or both unset",
        )

    if since is not None and until <= since:
        raise HTTPException(
            422,
            "since date must be strictly before until date",
        )

    return crud.subscriptions(db=db, campaign=campaign,
                              since=since, until=until,
                              customer_key=customer_key)
