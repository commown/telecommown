from datetime import datetime
from typing import Dict, List
from enum import Enum

from pydantic import BaseModel


class UserBase(BaseModel):
    login: str


class UserCreate(UserBase):
    pass


class User(UserBase):
    id: int

    class Config:
        orm_mode = True


class CampaignBase(BaseModel):
    creation_ts: datetime = None
    ref: str
    display_name: str = None
    description: str = None
    start_ts: datetime
    end_ts: datetime = None


class CampaignCreate(CampaignBase):
    member_ids: List[int] = []


class Campaign(CampaignBase):
    id: int
    members: List[User] = []

    class Config:
        orm_mode = True


class SubscriptionBase(BaseModel):
    customer_key: str


class SubscriptionOptIn(SubscriptionBase):
    optin_ts: datetime


class SubscriptionOptOut(SubscriptionBase):
    optout_ts: datetime


class Subscription(SubscriptionBase):
    id: int
    optin_ts: datetime
    optout_ts: datetime = None
    campaign: Campaign
    member: User

    class Config:
        orm_mode = True


class SubscriptionDates(BaseModel):
    optin_ts: datetime
    optout_ts: datetime = None


class SubscriptionEventType(str, Enum):
    optin = "optin"
    optout = "optout"


class SubscriptionEvent(BaseModel):
    type: SubscriptionEventType
    ts: datetime


class SubscriptionByCustomerKey(BaseModel):
    customer_key: str
    details: Dict[str, SubscriptionDates]
    events: List[SubscriptionEvent]
