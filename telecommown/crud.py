from datetime import datetime

from sqlalchemy.orm import Session

from . import models, schemas
from .database import addModelInstance


class InvalidDataError(Exception):
    pass


def get_users(db: Session):
    return db.query(models.User).all()


def get_user(db: Session, user_id: int):
    return db.query(models.User).filter(models.User.id == user_id).first()


def get_user_by_login(db: Session, login: str):
    return db.query(models.User).filter(models.User.login == login).first()


def create_user(db: Session, user: schemas.UserCreate):
    return addModelInstance(db, models.User(login=user.login))


def get_campaigns(db: Session):
    return db.query(models.Campaign).all()


def get_campaign_by_ref(db: Session, campaign_ref: str):
    return db.query(models.Campaign).filter(
        models.Campaign.ref == campaign_ref).first()


def create_campaign(db: Session, campaign: schemas.CampaignCreate):
    attrs = campaign.dict()
    attrs['members'] = [get_user(db, user_id)
                        for user_id in attrs.pop("member_ids", ())]
    return addModelInstance(db, models.Campaign(**attrs))


def create_subscription(
        db: Session,
        subscription: schemas.SubscriptionOptIn,
        campaign: schemas.Campaign,
        member: schemas.User,
):
    attrs = subscription.dict()
    attrs["campaign_id"] = campaign.id
    attrs["member_id"] = member.id
    subscription = models.Subscription(**attrs)

    if subscription.optin_ts >= campaign.end_ts:
        raise InvalidDataError("Opt-in date must be before campaign end")

    return addModelInstance(db, subscription)


def opt_out_subscription(
        db: Session,
        subscription: schemas.SubscriptionOptOut,
        campaign: schemas.Campaign,
        member: schemas.User,
):
    existing_subscription = db.query(models.Subscription).filter(
        models.Subscription.customer_key == subscription.customer_key,
        models.Subscription.campaign_id == campaign.id,
        models.Subscription.member_id == member.id,
    ).first()

    if not existing_subscription:
        raise InvalidDataError("Cannot find given subscription")
    elif existing_subscription.optout_ts:
        raise InvalidDataError("Subscription is already opt-out")
    elif not (campaign.start_ts <= subscription.optout_ts <= campaign.end_ts):
        raise InvalidDataError("Invalid opt-out date: out of campaign dates")
    elif (not existing_subscription.optin_ts
          or existing_subscription.optin_ts > subscription.optout_ts):
        raise InvalidDataError("Invalid opt-out date: not after opt-in date")
    else:
        existing_subscription.optout_ts = subscription.optout_ts
        db.commit()
        db.refresh(existing_subscription)
        return existing_subscription


def subscriptions(
        db: Session,
        campaign: schemas.Campaign,
        since: datetime = None,
        until: datetime = None,
        customer_key: str = None,
):
    since = max(since or campaign.start_ts, campaign.start_ts)
    until = min(until or campaign.end_ts, campaign.end_ts)

    subscriptions = db.query(models.Subscription).filter(
        models.Subscription.campaign_id == campaign.id
    ).filter(
        models.Subscription.optin_ts >= since
    ).filter(
        models.Subscription.optin_ts < until
    )

    if customer_key is not None:
        subscriptions = subscriptions.filter(
            models.Subscription.customer_key == customer_key
        )

    return subscriptions.all()


def subscription_important_events(
        db: Session,
        campaign: schemas.Campaign,
        since: datetime = None,
        until: datetime = None,
        customer_key: str = None,
):
    since = max(since or campaign.start_ts, campaign.start_ts)
    until = min(until or campaign.end_ts, campaign.end_ts)

    subscriptions = db.query(models.Subscription).filter(
        models.Subscription.campaign_id == campaign.id
    )
    if customer_key:
        subscriptions = subscriptions.filter(
            models.Subscription.customer_key == customer_key
        )

    result = {}
    for subscription in subscriptions:
        key = subscription.customer_key
        data = result.setdefault(key, {
            "customer_key": key,
            "details": {},
            "events": [],
        })
        data["details"][subscription.member.login] = {
            "optin_ts": subscription.optin_ts,
            "optout_ts": subscription.optout_ts,
        }

    for key in list(result):
        partners_data = result[key]["details"]

        if len(partners_data) != len(campaign.members):
            # no global optin for this customer
            del result[key]
            continue

        max_optin_ts = max(v["optin_ts"] for v in partners_data.values())
        if since < max_optin_ts <= until and max_optin_ts >= campaign.start_ts:
            result[key]["events"].append({"type": "optin",
                                          "ts": max_optin_ts})

        min_optout_ts = min(v["optout_ts"] or campaign.end_ts
                            for v in partners_data.values())
        if since < min_optout_ts <= until:
            result[key]["events"].append({"type": "optout",
                                          "ts": min_optout_ts})

        if not result[key]["events"]:
            # no important event for this customer
            del result[key]

    return list(result.values())
