from sqlalchemy import (Table, Column, ForeignKey, UniqueConstraint,
                        Integer, DateTime, Text)
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from .database import Base


campaign_members = Table(
    'campaign_members',
    Base.metadata,
    Column('user_id', ForeignKey('users.id')),
    Column('campaign_id', ForeignKey('campaigns.id'))
)


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    login = Column(Text, unique=True, index=True)


class Campaign(Base):
    __tablename__ = "campaigns"

    id = Column(Integer, primary_key=True)
    creation_ts = Column(DateTime(timezone=True), default=func.now())
    ref = Column(Text, unique=True, index=True)
    display_name = Column(Text)
    description = Column(Text)
    start_ts = Column(DateTime(timezone=True), index=True)
    end_ts = Column(DateTime(timezone=True), index=True)
    members = relationship("User", secondary=campaign_members)


class Subscription(Base):
    __tablename__ = "subscriptions"
    __table_args__ = (
        UniqueConstraint('customer_key', 'campaign_id', 'member_id'),
        {}
    )

    id = Column(Integer, primary_key=True)
    customer_key = Column(Text, index=True)
    campaign_id = Column(Integer, ForeignKey("campaigns.id"))
    campaign = relationship("Campaign")
    member_id = Column(Integer, ForeignKey("users.id"))
    member = relationship("User")
    optin_ts = Column(DateTime(timezone=True), nullable=False)
    optout_ts = Column(DateTime(timezone=True))
