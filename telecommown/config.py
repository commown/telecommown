"See https://fastapi.tiangolo.com/advanced/settings/?h=setting"

from pydantic import BaseSettings


class Settings(BaseSettings):

    db_host: str = ""
    db_port: int = 5432
    db_name: str
    db_user: str
    db_password: str = ""

    class Config:
        env_file = ".env"


settings = Settings()
