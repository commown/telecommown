from sqlalchemy_utils import create_database

from .database import engine
from .models import Base


def init_db(insert_data_func=None):

    create_database(engine.url)
    Base.metadata.create_all(bind=engine)
    if insert_data_func is not None:
        insert_data_func(engine)
