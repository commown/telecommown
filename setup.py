from setuptools import setup, find_packages


setup(
    name='telecommown',
    version='0.0.1',
    description='Web services to manage shared campaigns at Telecoop/Commown',
    packages=find_packages(where='telecommown'),
    python_requires='>=3.7, <4',
    scripts=['bin/tc_init'],
)
