# Telecommown

Services web permettant de créer des campagnes communes entre
plusieurs organisations, auxquelles peuvent souscrire leurs
client.e.s.

Lorsqu'une personne (client.e) a souscrit à la campagne auprès de
chaque organisation, elle peut alors prétendre aux avantages prévus
par l'ensemble (réduction de tarif par exemple).

Ces services web permettent de définir des campagnes et leurs
organisations partenaires, et partager l'avancement des souscriptions
des personnes auprès des organisations de la campagne. Les personnes
sont identifiées par hâchage de données personnelles possédant
suffisamment d'entropie (par exemple un numéro de téléphone + un IBAN)
pour ne rien dévoiler aux organisations qui ne possèderaient pas déjà
cette donnée.

## Configuration de la connexion à la base de données

La façon la plus simple est d'écrire un fichier nommé ".env" dans le
dossier utilisé pour lancer l'application. Ce fichier doit contenir
les informations nécessaires à la connexion à la base de données SQL
qui stocke les informations nécessaires au fonctionnement du service :
`
db_name=<nom de la base de données>
db_user=<nom de l'utilisateur de la BDD>
db_password=<mot de passe de l'utilisateur (optionnel, vide par défaut)>
db_host=<nom d'hôte ou IP du serveur de BDD (optionnel, vide par défaut)>
db_port=<numéro du port d'écoute du serveur de BDD (optionnel, 5342 par défaut)>
`

À noter que pour faire tourner les tests unitaires, il suffit de
fournir un tel fichier .env dans le répertoire des tests.

## Usage

Les services sont auto-documentés sur le chemin /docs (par exemple en
développement sur l'URL http://127.0.0.1:8000/docs).

Le déroulé global est le suivant :

- créer les organisations membres de la campagne grâce à des requêtes
  POST sur "/users/"

- créer une campagne avec un POST sur "/campaigns/"

- lorsqu'un utilisateur souscrit à une campagne chez une organisation
  membre données, effectuer un POST sur
  "/campaigns/{campaign_ref}/opt-in"

- si la souscription est annulée d'une personne prend fin, effectuer
  un POST sur "/campaigns/{campaign_ref}/opt-out"

- enfin, chaque organisation interroge régulièrement par un GET le
  service "/subscriptions/", ce qui permet d'attribuer les avantages
  de la campagne à une personne (lorsqu'elle a souscrit auprès de
  toutes les organisations membres) ou de les ôter (lorsque l'une au
  moins des souscriptions de cette personne à cette campagne a pris
  fin).

## Configuration derrière nginx

Exemple de fichier de configuration de nginx (http seul ici) comme
reverse proxy :

```
server {
    listen 80;
    server_name tcapi.commown.coop;

    location / {
      auth_basic "Restricted";
      auth_basic_user_file /usr/share/nginx/.htpasswd;

      sendfile off;

      proxy_set_header Authorization "";
      proxy_set_header X-Forwarded-User $remote_user;
      proxy_set_header Host $http_host;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto $scheme;

      proxy_redirect off;
      proxy_buffering off;

      proxy_pass http://127.0.0.1:8000;
    }

}
```
